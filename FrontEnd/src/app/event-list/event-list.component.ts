import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {
  gantiwaktu= [];
  constructor(private api:ApiService) { }
  list:Object[];
  ngOnInit() {
    this.api.eventlist().subscribe(result => this.list = result);
  }
  beliTiket(id){
    this.api.beliTiket(id);
  }
  gantiWaktu(id,i){
    this.api.gantiWaktu(id,this.gantiwaktu[i]);
  }

}
