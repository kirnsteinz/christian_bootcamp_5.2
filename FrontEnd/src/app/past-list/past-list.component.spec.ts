import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastListComponent } from './past-list.component';

describe('PastListComponent', () => {
  let component: PastListComponent;
  let fixture: ComponentFixture<PastListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
