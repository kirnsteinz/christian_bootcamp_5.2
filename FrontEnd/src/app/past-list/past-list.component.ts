import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-past-list',
  templateUrl: './past-list.component.html',
  styleUrls: ['./past-list.component.css']
})
export class PastListComponent implements OnInit {

  constructor(private api:ApiService) { }
  list:Object[];
  ngOnInit() {
    this.api.alllist().subscribe(result => this.list = result);
  }

}
