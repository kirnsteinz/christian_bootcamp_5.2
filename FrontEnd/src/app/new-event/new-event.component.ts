import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-new-event',
  templateUrl: './new-event.component.html',
  styleUrls: ['./new-event.component.css']
})
export class NewEventComponent implements OnInit {

  nama;
  waktu;
  detail;
  harga;
  constructor(private api:ApiService) { }

  ngOnInit() {
  }
  eventBaru(){
    this.api.eventBaru(this.nama,this.waktu,this.harga,this.detail);
  }
}
