import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from "@angular/router";

import { AppComponent } from './app.component';
import { EventListComponent } from './event-list/event-list.component';
import { NewEventComponent } from './new-event/new-event.component';
import { ApiService } from './api.service';
import { PastListComponent } from './past-list/past-list.component';

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    NewEventComponent,
    PastListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      		{path: '', component: EventListComponent},
      		{path: 'newevent', component: NewEventComponent},
          {path: 'past', component: PastListComponent}
    		])
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
