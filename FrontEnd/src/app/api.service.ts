import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ApiService {

  constructor(private http:Http, private router:Router) { }
  eventlist(){
    return this.http.get('http://localhost:8000/api/')
    .map(result => result.json());
  }
  alllist(){
    return this.http.get('http://localhost:8000/api/all')
    .map(result => result.json());
  }
  eventBaru(nama,waktu,harga,detail){
    let data = {
      "nama" : nama,
      "waktu" : waktu,
      "harga" : harga,
      "detail" : detail
    };
    let b = JSON.stringify(data);
    let h = new Headers({"Content-Type" : "application/json"});
    let o = new RequestOptions({headers: h});
    console.log(b,o);
    this.http.post('http://localhost:8000/api/eventbaru',b,o); 
  }
  beliTiket(id){
  let data = {
      "id" : id
    };
    let b = JSON.stringify(data);
    let h = new Headers({"Content-Type" : "application/json"});
    let o = new RequestOptions({headers: h});
    console.log(b,o);
    this.http.post('http://localhost:8000/api/tiket',b,o);  
  }
  gantiWaktu(id,gantiwaktu){
  let data = {
      "id" : id,
      "waktu" : gantiwaktu
    };
    let isi = JSON.stringify(data);
    let h = new Headers({"Content-Type" : "application/json"});
    let o = new RequestOptions({headers: h});
    console.log(isi,o);
    this.http.post('http://localhost:8000/api/gantiwaktu',isi,o);  
  }
}
