<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Event;

class eventController extends Controller
{   
    function index(){
        $list = Event::all();
        return $list;
    }
    function event(){
        $date = date('Y-m-d');
        $list = Event::where('waktu','>=',$date)->get();
        return $list;
    }
    function eventBaru(Request $request){
      DB::beginTransaction();
        try{
            $this->validate($request, [
                'nama' => 'required',
                'waktu' => 'required',
                'harga' => 'required'
            ]);            
            $new = new Event;
            $new->nama = $request->input('nama');
            $new->waktu = $request->input('waktu');
            $new->harga = $request->input('harga');
            $new->detail = $request->input('detail');
            $new->terjual = 0;
            $new->save();

            DB::commit();
            return response()->json($new, 201);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json([$e->getMessage()], 500);
        }
    }
    function gantiWaktu(Request $request){
      DB::beginTransaction();
        try{
            $this->validate($request, [
                'id' => 'required',
                'waktu' => 'required'
            ]);            
            $new = Event::where('id',$request->input('id'))->get()->first();
            $new->waktu = $request->input('waktu');
            $new->save();

            DB::commit();
            return response()->json($new, 201);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json([$e->getMessage()], 500);
        }
    }
    function tiketTerjual(Request $request){
      DB::beginTransaction();
        try{
            $this->validate($request, [
                'id' => 'required'
            ]);            
            $new = Event::where('id',$request->input('id'))->get()->first();
            $new->terjual++;
            $new->save();

            DB::commit();
            return response()->json($new, 201);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json([$e->getMessage()], 500);
        }
    }
}
