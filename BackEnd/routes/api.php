<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/','eventController@event');
Route::get('/all','eventController@index');
Route::post('/eventbaru','eventController@eventbaru');
Route::post('/gantiwaktu','eventController@gantiWaktu');
Route::post('/tiket','eventController@tiketTerjual');